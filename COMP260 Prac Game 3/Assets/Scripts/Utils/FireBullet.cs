﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {
	public BulletMove bulletPrefab;
	public float reload = 0.0f;
	public float shootTimer = 0.5f;
	void Update () {
		// when the button is pushed, fire a bullet
		shootTimer += Time.deltaTime;
		if (shootTimer < reload) {
			return;
		}
		if (Input.GetButtonDown ("Fire1")) {
			
			BulletMove bullet = Instantiate (bulletPrefab);
			// the bullet starts at the player's position
			bullet.transform.position = transform.position;

			// create a ray towards the mouse location
			Ray ray = 
				Camera.main.ScreenPointToRay (Input.mousePosition);
			bullet.direction = ray.direction;
			shootTimer = 0;
		}
	}
}
